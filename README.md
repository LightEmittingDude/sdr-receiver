# SDR Receiver

This module implements a FM demodulator using an RTL-SDR connected to a Raspberry Pi. It uses Python's rtlsdr library.

